# Data Sets

A description of the corresponsing data sets can be found in the papers mentioned below

## Maximum Capture Problem
Haase, K., Müller, S. (2014): A comparison of linear reformulations for multinomial logit choice probabilities in facility location models. European Journal of Operational Research 232(3), 689–691.

